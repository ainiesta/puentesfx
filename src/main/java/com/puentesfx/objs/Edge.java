/** 
 * Copyright [2013] Antonio J. Iniesta
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 * File created: 23/07/2013 at 00:36:34 by antonio
 */
package com.puentesfx.objs;

/**
 * @author antonio
 * 
 */
public class Edge<T> {

	private Vertice<?> origin;
	private Vertice<?> destination;
	private T value;

	/**
	 * @param origin
	 * @param destination
	 */
	public Edge(Vertice<?> origin, Vertice<?> destination) {
		this(origin, destination, null);
	}

	/**
	 * @param origin
	 * @param destination
	 * @param value
	 */
	public Edge(Vertice<?> origin, Vertice<?> destination, T value) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.value = value;
	}

	public void newVertices(Vertice<?> origin, Vertice<?> destination) {
		this.origin = origin;
		this.destination = destination;
	}

	/**
	 * @return the origin
	 */
	public Vertice<?> getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(Vertice<?> origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public Vertice<?> getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(Vertice<?> destination) {
		this.destination = destination;
	}

	/**
	 * @return the value
	 */
	public T getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(T value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Edge))
			return false;
		@SuppressWarnings("rawtypes")
		Edge other = (Edge) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Edge [origin=" + origin + ", destination=" + destination + ", value=" + value + "]";
	}

}
