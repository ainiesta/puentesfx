/** 
 * Copyright [2013] Antonio J. Iniesta
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * 
 * File created: 23/07/2013 at 00:37:36 by antonio
 */
package com.puentesfx.objs;

import java.util.List;
import java.util.Set;

/**
 * @author antonio
 * 
 *         The basic operations provided by a graph data structure G usually
 *         include:
 * 
 *         adjacent(G, x, y): tests whether there is an edge from node x to node
 *         y. neighbors(G, x): lists all nodes y such that there is an edge from
 *         x to y. add(G, x, y): adds to G the edge from x to y, if it is not
 *         there. delete(G, x, y): removes the edge from x to y, if it is there.
 * 
 *         get_node_value(G, x): returns the value associated with the node x.
 * 
 *         set_node_value(G, x, a): sets the value associated with the node x to
 *         a. Structures that associate values to the edges usually also
 *         provide: get_edge_value(G, x, y): returns the value associated to the
 *         edge (x,y). set_edge_value(G, x, y, v): sets the value associated to
 *         the edge (x,y) to v.
 * 
 */
public class Graph {

	private Set<Vertice<?>> vertices;
	private Set<Edge<?>> edges;

	/**
	 * @param vertices
	 * @param edges
	 */
	public Graph(Set<Vertice<?>> vertices, Set<Edge<?>> edges) {
		super();
		this.vertices = vertices;
		this.edges = edges;
	}

	public boolean isAdjacent(Vertice<?> x, Vertice<?> y){
		//TODO
		return false;
	}
	
	public List<Vertice<?>> getNeighbors(){
		//TODO
		return null;
	}
	
	/**
	 * @return the vertices
	 */
	public Set<Vertice<?>> getVertices() {
		return vertices;
	}

	/**
	 * @param vertices
	 *            the vertices to set
	 */
	public void setVertices(Set<Vertice<?>> vertices) {
		this.vertices = vertices;
	}

	/**
	 * @return the edges
	 */
	public Set<Edge<?>> getEdges() {
		return edges;
	}

	/**
	 * @param edges
	 *            the edges to set
	 */
	public void setEdges(Set<Edge<?>> edges) {
		this.edges = edges;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edges == null) ? 0 : edges.hashCode());
		result = prime * result + ((vertices == null) ? 0 : vertices.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Graph))
			return false;
		Graph other = (Graph) obj;
		if (edges == null) {
			if (other.edges != null)
				return false;
		} else if (!edges.equals(other.edges))
			return false;
		if (vertices == null) {
			if (other.vertices != null)
				return false;
		} else if (!vertices.equals(other.vertices))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Graph [vertices=" + vertices + ", edges=" + edges + "]";
	}
}
